/*-- ------------------------------------------------------------ 
###################################################################### 
#Copyright (C) 2018  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

*/

var camera, scene, renderer, controls, raycaster, player,keys = [];
var clock = new THREE.Clock();
var clockD;

init();
animate();

function init() {
  scene = new THREE.Scene();

  createRenderer();
  createCamera();



  player = createCube(1);
  player.key_forward = "w";
  player.key_back = "s";
  player.key_left = "a";
  player.key_right = "d";
  player.update = player_update;

  player2 = createCube(1);
  player2.key_forward = "u";
  player2.key_back = "j";
  player2.key_left = "h";
  player2.key_right = "k";
  player2.update = player_update;
  player2.position.x = 10;

  floor = createPlane();
  floor.rotation.x = Math.PI /2;//rotate 90 degrees
  floor.position.y = -.5;
  light = createLights();
  window.addEventListener( 'resize', onWindowResize, false );
  document.addEventListener('keydown', keystate); 
  document.addEventListener('keyup', keystate); 
}

function keystate(ev){
  if ( ev.type == "keydown" ){
    keys[ev.key] = true; 
  }else if(ev.type == "keyup"){
    keys[ev.key] = false;
  }
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
  requestAnimationFrame( animate );
  renderer.render( scene, camera );

  clockD = clock.getDelta(); 
  //uncoment to have camera look at player 
  //controls.target = player.position; 
  controls.update();
  player.update();
  player2.update();
  //TWEEN.update();
}

function player_update(){
  var moveDistance = 5 * clockD; 
  //turn
  if ( keys[this.key_left] ){this.rotateY( moveDistance )};
  if ( keys[this.key_right] ){this.rotateY( -moveDistance )};

  //move
  if ( keys[this.key_forward] ){this.translateZ( moveDistance );};
  if ( keys[this.key_back] ){this.translateZ( -moveDistance );};
}

function createCamera(){
  camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 5000 );
  camera.position.z = -5;
  camera.position.y = 4;
  camera.lookAt(scene.position);

  //addcontrols
  controls = new THREE.OrbitControls( camera, renderer.domElement );
}

function createRenderer(){
  renderer = new THREE.WebGLRenderer( { antialias: true,alpha: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

}

function createCube(size){
  var geometry = new THREE.BoxGeometry( size, size, size );
  var material = new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } );
  var cube = new THREE.Mesh( geometry, material );
  cube.position.set( 0, 0, 0 );
  scene.add(cube);
  return cube;
} 


function createLights(){
  //set to true to view light positions
  var helpers = false;

  light1 = new THREE.DirectionalLight( 0xffffff, 1 );
  light1.position.set( 5, 5, 10 ).normalize();
  scene.add( light1 );

  //Add helper to view light position
  if(helpers){
    light1.helper = new THREE.DirectionalLightHelper( light1, 50 );
    scene.add( light1.helper);
  }

  light2 = new THREE.DirectionalLight( 0xffffff, 1 );
  light2.position.set(-1,-1,-5).normalize();
  scene.add( light2 );

  //Add helper to view light position
  if(helpers){
    light2.helper = new THREE.DirectionalLightHelper( light2, 50 );
    scene.add( light2.helper);
  }

  return true;
}

function createPlane(){
  //PlaneGeometry(width : Float, height : Float, widthSegments : Integer, heightSegments : Integer)
  var geometry = new THREE.PlaneGeometry( 20, 20, 32 );
  var material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
  var plane = new THREE.Mesh( geometry, material );
  scene.add( plane );

  return plane;
}

