  //jump                          
  if ( keys[this.key_jump] && this.position.y <= 0){
    this.jump = true;             
    setTimeout(function(player){player.jump = false},200,this);
  };                                                                
                                                                                                                                         
  if (this.position.y > 0){                                                                                                              
    if(!this.jump){                                                 
      this.translateY( -this.gravity );                                                                                                  
    }                                                               
  }else if(this.position.y < 0){
    this.position.y = 0;
  }                               
                                  
  if(this.jump){                                                    
      this.translateY( this.gravity );                                                                                                   
  }
